package com.example.hakatonbackend.model;

import static javax.persistence.EnumType.STRING;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

	private static final long serialVersionUID = -1L;

	private String username;

	private String password;

	@Enumerated(STRING)
	private Role role;

}