package com.example.hakatonbackend.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Desk extends BaseEntity {

	private static final long serialVersionUID = -1L;

	private String description;

	@OneToMany
	private List<Period> periods;

}