package com.example.hakatonbackend.model;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = -1L;

	@Id
	@GeneratedValue
	private Long id;

}