package com.example.hakatonbackend.model;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Period extends BaseEntity {

	private static final long serialVersionUID = -1L;

	private LocalDateTime startDate;

	private LocalDateTime endDate;

	@OneToOne(optional = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private User user;

}