package com.example.hakatonbackend.service;

import com.example.hakatonbackend.model.Room;
import com.example.hakatonbackend.repository.RoomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomService {

	private final RoomRepository roomRepository;

	@Autowired
	public RoomService(RoomRepository roomRepository) {
		this.roomRepository = roomRepository;
	}

	public List<Room> findAll() {
		return roomRepository.findAll();
	}

	public Long save(Room room) {
		roomRepository.save(room);
		return room.getId();
	}

}
