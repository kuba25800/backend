package com.example.hakatonbackend.service;

import com.example.hakatonbackend.model.User;
import com.example.hakatonbackend.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	private final UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public Long save(User user) {
		userRepository.save(user);
		return user.getId();
	}
}
