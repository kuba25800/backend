package com.example.hakatonbackend.repository;

import com.example.hakatonbackend.model.Period;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeriodRepository extends JpaRepository<Period, Long> {
}
