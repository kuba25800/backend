package com.example.hakatonbackend.repository;

import com.example.hakatonbackend.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
