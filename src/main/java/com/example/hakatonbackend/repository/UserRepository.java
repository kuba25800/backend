package com.example.hakatonbackend.repository;

import com.example.hakatonbackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
