package com.example.hakatonbackend.repository;

import com.example.hakatonbackend.model.Desk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeskRepository extends JpaRepository<Desk, Long> {
}
