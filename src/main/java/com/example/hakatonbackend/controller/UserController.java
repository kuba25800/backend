package com.example.hakatonbackend.controller;

import com.example.hakatonbackend.model.User;
import com.example.hakatonbackend.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/users")
	public List<User> users() {
		return userService.findAll();
	}

	@PostMapping("/user/create")
	public Long createUser(@RequestBody User user) {
		return userService.save(user);
	}

}
