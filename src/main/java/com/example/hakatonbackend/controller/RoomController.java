package com.example.hakatonbackend.controller;

import com.example.hakatonbackend.model.Room;
import com.example.hakatonbackend.service.RoomService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RoomController {

	private final RoomService roomService;

	@Autowired
	public RoomController(RoomService roomService) {
		this.roomService = roomService;
	}

	@GetMapping("/rooms")
	public List<Room> rooms() {
		return roomService.findAll();
	}

	@PostMapping("/room/create")
	public Long createRoom(@RequestBody Room room) {
		return roomService.save(room);
	}

}
