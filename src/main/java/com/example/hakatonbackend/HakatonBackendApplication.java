package com.example.hakatonbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HakatonBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HakatonBackendApplication.class, args);
	}

}
